import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)  # Загружаем датасет MNIST

batch_size = 64  # Количество изобрадажений, закачиваемых в нейросеть за раз
latent_space = 128  # Количество нейронов в скрытом слое
leaning_rate = 0.1  # Шаг градиентного спуска
iterations = 1000  # Количество циклов обучения

# Веса скрытого слоя
w1 = tf.Variable(tf.truncated_normal([784, latent_space], stddev=0.1))
b1 = tf.Variable(tf.truncated_normal([latent_space], stddev=0.1))

# Веса выходного слоя
w2 = tf.Variable(tf.zeros([latent_space, 10]))
b2 = tf.Variable(tf.zeros([10]))

# Входные данные
x = tf.placeholder(tf.float32, [None, 784])

# Скрытый слой
hidden = tf.nn.relu(tf.matmul(x, w1) + b1)

# Выходной слой
y = tf.nn.softmax(tf.matmul(hidden, w2) + b2)

# Данные для проверки
y_ = tf.placeholder(tf.float32, [None, 10])

# Функция проверки
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
train_step = tf.train.GradientDescentOptimizer(leaning_rate).minimize(cross_entropy)
accurary = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(y, 1), tf.arg_max(y_, 1)), tf.float32))

# Создание сессии Tensorflow
init = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init)

# Обучение
for i in range(iterations):
    xs, ys = mnist.train.next_batch(batch_size)
    sess.run(train_step, feed_dict={x: xs, y_: ys})

# Вывод результатов на экран
res = sess.run(accurary, feed_dict={x: mnist.test.images, y_: mnist.test.labels})
print("Точность: {0}".format(res))
